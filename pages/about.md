---
layout: page
permalink: /about/
title: About me
subtitle: a nerd, but like a cool nerd
---

Howdy, 🤠 I'm Kyanne! I love computers and I love the Internet. I'm teaching myself how to program so I can test the limits of computing and to help develop technology that keeps our Internet open, accessible, and uncensored.

I am a privacy advocate, an open source enthusiast, and a [wannabe hacker](http://www.catb.org/jargon/html/W/wannabee.html). Apart from computers, I'm a fan of my dogs, watching horror movies, and reading.

Find me on [Twitter][twitter], [Github][github] or [Linkedin][linkedin]

<!-- Feel free to contact me at [sonny.webdsg at gmail dot com][email]. -->


[me]: /assets/images/me.jpg
[email]: mailto:sonny.webdsg[at]gmail[dot]com
[twitter]: https://twitter.com/kyr0se
[github]: https://github.com/kyrose
[linkedin]: https://www.linkedin.com/in/kyannerose
